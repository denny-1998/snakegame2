import javafx.scene.control.Label;

public class ScoreUpdater implements Runnable {
    private Label scoreLabel;
    private int score;

    public ScoreUpdater(Label scoreLabel){
        this.scoreLabel=scoreLabel;
        score=0;
    }

    public void incrementScore(){
        score++;
    }
    @Override
    public void run() {
        scoreLabel.setText("Your score is: "+score);
    }
}
