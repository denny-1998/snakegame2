import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;

public class Snake {
    private Position pos;
    private ArrayList<Position> previousPositions;
    private static Dir direction = Dir.left;

    public enum Dir{
        left,right,up,down;
    }

    public Snake(Board b, GraphicsContext gc,Scene scene) {
        //start at random position
        int randX = ThreadLocalRandom.current().nextInt(1,  b.getTiles());
        int randY = ThreadLocalRandom.current().nextInt(1,  b.getTiles());

        //put coordinates into position object
        pos = new Position(randX, randY);

        gc.setFill(Color.ORANGE);
        gc.fillOval(randX * b.getTilesize(), randY * b.getTilesize(), b.getTilesize(), b.getTilesize());

        //create previous positions arrayList
        previousPositions = new ArrayList<>();

        scene.addEventFilter(KeyEvent.KEY_PRESSED, key->{
            if((key.getCode()== KeyCode.W)&&(direction!=Dir.down)){
                direction = Dir.up;
            }
            if((key.getCode()== KeyCode.A)&&(direction!=Dir.right)){
                direction = Dir.left;
            }
            if((key.getCode()== KeyCode.D)&&(direction!=Dir.left)){
                direction = Dir.right;
            }
            if((key.getCode()== KeyCode.S)&&(direction!=Dir.up)){
                direction = Dir.down;
            }
        });
    }

    public Position getPos (){
        return this.pos;
    }

    public void move(GraphicsContext gc,Board b){
        previousPositions.add(pos);
        System.out.println(previousPositions.size());
        for(Position pos:previousPositions){
            Square square = b.getSquares(pos.getX(),pos.getY());
            gc.setFill(square.getColor());
            gc.fillRect(square.getX(),square.getY(),b.getTilesize(),b.getTilesize());
        }

        switch (direction){
            case up:
                pos = new Position(pos.getX(), pos.getY()-1);
                break;
            case down:
                pos = new Position(pos.getX(), pos.getY()+1);
                break;
            case left:
                pos = new Position(pos.getX()-1, pos.getY());
                break;
            case right:
                pos = new Position(pos.getX()+1, pos.getY());
                break;
        }

        previousPositions.remove(0);

        gc.setFill(Color.ORANGE);
        for(Position p:previousPositions){
            gc.fillOval(p.getX() * b.getTilesize(), p.getY() * b.getTilesize(), b.getTilesize(), b.getTilesize());
        }
        gc.fillOval(pos.getX()*b.getTilesize(),pos.getY()*b.getTilesize(),b.getTilesize(),b.getTilesize());
    }

    public void grow(){
        if(previousPositions.size()==0){
            previousPositions.add(pos);
        }else{
            previousPositions.add(0,previousPositions.get(previousPositions.size()-1));
        }
        for(Position p:previousPositions){
            System.out.print(p.getX()+" "+p.getY()+",");
        }
    }

    public boolean checkCollision(){
        if(previousPositions.size()<4)
            return false;
        for(Position p:previousPositions)
            if(p.equals(pos))
                return true;

        return false;
    }

    public ArrayList<Position> getPositions(){
        ArrayList<Position> positions=new ArrayList<>();
        for(Position p:previousPositions){
            positions.add(p);
        }
        positions.add(pos);
        return positions;
    }
}
