import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();

        Canvas canvas=new Canvas(600,600);
        canvas.setLayoutX(20);
        canvas.setLayoutY(40);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Label scoreLabel=new Label("Your score is: 0");
        scoreLabel.setLayoutX(30);
        scoreLabel.setLayoutY(10);

        root.getChildren().addAll(canvas,scoreLabel);
        primaryStage.setScene(new Scene(root, 640, 660));
        primaryStage.show();

        Board b = new Board(gc);

        startGame(b,gc, primaryStage.getScene(),scoreLabel);
    }

    public void startGame(Board b, GraphicsContext gc, Scene scene,Label scoreLabel) {
        Thread thread= new Thread(() -> {
            ScoreUpdater scoreUpdater=new ScoreUpdater(scoreLabel);
            Snake snake=new Snake(b, gc, scene);
            Food food=new Food(gc,b,snake.getPositions());
            do{
                snake.move(gc,b);
                if(food.getPos().equals(snake.getPos())){
                    scoreUpdater.incrementScore();
                    food.delete();
                    food = new Food(gc, b,snake.getPositions());
                    gc.setFill(Color.ORANGE);
                    gc.fillOval(snake.getPos().getX()*b.getTilesize(),snake.getPos().getY()*b.getTilesize(),b.getTilesize(),b.getTilesize());
                    snake.grow();
                    Platform.runLater(scoreUpdater);
                }
                try{
                    Thread.sleep(200);
                }catch(Exception e){}
            }while(!invalidPos(snake,b));
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Game over!");
                alert.showAndWait();
            });
        });

        thread.start();
    }


    public boolean invalidPos(Snake snake, Board b){
        if(snake.getPos().getY() <= 0){
            return true;
        } else if (snake.getPos().getY() >= b.getTiles()){
            return true;
        } else if (snake.getPos().getX() <= 0){
            return true;
        } else if (snake.getPos().getX() >= b.getTiles()){
            return true;
        } else {
            return snake.checkCollision();
        }
    }
}
