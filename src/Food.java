import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Food {
    private Position pos;
    private GraphicsContext gc;
    private Board b;

    public Food(GraphicsContext gc, Board b, ArrayList<Position> positions){
        this.gc = gc;
        this.b = b;

        boolean valid;
        do{
            int randomX = ThreadLocalRandom.current().nextInt(1,  b.getTiles());
            int randomY = ThreadLocalRandom.current().nextInt(1,  b.getTiles());
            pos = new Position(randomX, randomY);

            valid=true;
            for(Position p:positions){
                if(p.equals(pos)){
                    valid=false;
                    break;
                }
            }
        }while(!valid);


        gc.setFill(Color.RED);
        gc.fillOval(pos.getX() * b.getTilesize(), pos.getY() * b.getTilesize(), b.getTilesize(), b.getTilesize());

    }

    public Position getPos(){
        return pos;
    }

    public void delete(){
        Square currentPos = b.getSquares(pos.getX(), pos.getY());
        gc.setFill(currentPos.getColor());
        gc.fillRect(pos.getX() * b.getTilesize(), pos.getY() * b.getTilesize(), b.getTilesize(), b.getTilesize());
    }
}
